#include "circle.h"

#ifdef __cplusplus
extern "C" {
#endif

void circle_decode(std::istream &is, std::ostream &os) {
	unsigned char prev = 0;
	unsigned char chr = 0;
	unsigned char pt = 0;
	while (is.good()) {
		chr = is.get();
		if (is.fail()) { return; }
		pt = (unsigned char)((prev + chr) % 0xff);
		os << pt;
		prev = pt;
	}
}

#ifdef __cplusplus
}
#endif

