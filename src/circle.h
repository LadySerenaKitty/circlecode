#include <iostream>
#include <string>
#include <fstream>
#include <iomanip>

#ifdef __cplusplus
extern "C" {
#endif

void circle_encode(std::istream &is, std::ostream &os);
void circle_decode(std::istream &is, std::ostream &os);
void circle_enhex(std::istream &is, std::ostream &os);
void circle_dehex(std::istream &is, std::ostream &os);

#ifdef __cplusplus
}
#endif

#ifndef LIBRARY
void codeloop(std::istream &is);
#endif

