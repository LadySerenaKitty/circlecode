#include "circle.h"

void nullcode(std::istream &is, std::ostream &os) {
	unsigned char chr = 0;
	while(is.good()) {
		chr = is.get();
		if (is.fail()) { return; }
		os << chr;
	}
}

bool contains(std::string in, std::string chk) {return (in.find(chk) != in.npos); }

int main(int argc, char** argv) {

	if (argc == 0) { return 1024; }

	void (*rawr)(std::istream &is, std::ostream &os);
	std::string mode(argv[0]);

	if      (contains(mode, "encode")) { rawr = &circle_encode; }
	else if (contains(mode, "decode")) { rawr = &circle_decode; }
	else { rawr = &nullcode; }

	if (argc <= 1) { (*rawr)(std::cin, std::cout); }
	else {
		std::ifstream is(argv[1], std::ios::binary | std::ios::in);
		(*rawr)(is, std::cout);
		is.close();
	}

	return 0;
}

