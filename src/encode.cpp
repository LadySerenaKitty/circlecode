#include "circle.h"

unsigned char encode_work(unsigned char a, unsigned char b) {
	if (a < b) { return b - a; }
	else if (a > b) { return 0xff - (a - b); }
	return 0;
}

#ifdef __cplusplus
extern "C" {
#endif

void circle_encode(std::istream &is, std::ostream &os) {
	os << std::setfill('0');
	unsigned char prev = 0;
	unsigned char chr = 0;
	while(is.good()) {
		chr = is.get();
		if (is.fail()) { return; }
		os << encode_work(prev, chr);
		prev = chr;
	}
}

#ifdef __cplusplus
}
#endif

