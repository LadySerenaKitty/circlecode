# Circles Encoding
---

Simple encoding system that uses progressive difference.  It is **not** crypto.  Two tools are included:
+ **circle-encode** - Encodes input
+ **circle-decode** - Decodes input


## Usage
---
Usage is simple and straightforward.  Without any arguments, it will read from `stdin`.  Only 1 argument is supported
and that would be an input file.  All output goes to `stdout`, so if you want the output to go to a file, use shell
redirection.


## Examples
---
```
echo "Mew mew" | circle-encode | enhex
```
Output:
```
4d 18 12 a8 4d f7 12 92
```
---
```
echo "4d 18 12 a8 4d f7 12 92" | dehex | circle-decode
```
Output:
```
Mew mew
```
---
```
circle-encode some-file | enhex > output.txt
```

